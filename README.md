# About avc-commons3-types

Common types used in Avantage Compris projects,
namely, dates + millis.

Projects that use this one include:

  * [avc-commons3-yaml](https://gitlab.com/avcompris/avc-commons3-yaml/)
  * [avc-commons3-databeans](https://gitlab.com/avcompris/avc-commons3-databeans/)


This is the project home page, hosted on GitLab.

[API Documentation is here](https://maven.avcompris.com/avc-commons3-types/apidocs/index.html).

There is a [Maven Generated Site.](https://maven.avcompris.com/avc-commons3-types/)

