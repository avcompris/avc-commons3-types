package net.avcompris.commons3.types;

import static org.joda.time.DateTimeZone.UTC;

import java.io.Serializable;

import org.joda.time.DateTime;

/**
 * A dual representation of a date.
 * Such an object holds two fields:
 * <ul>
 * <li> dateTime, as a ISO-8601 String, so it can be displayed in UIs
 * <li> timestamp, as long, so it can be used in comparisons
 * </ul>
 *
 * @author dandriana
 */
public interface DateTimeHolder extends Comparable<DateTimeHolder>, Serializable, Cloneable {

	/**
	 * The ISO-8601 representation of this date.
	 * @return the ISO-8601 representation of this date
	 */
	String getDateTime();

	/**
	 * The millisecond time of this date.
	 * @return the millisecond time of this date
	 */
	long getTimestamp();

	/**
	 * Compare two {@link DateTimeHolder} objects.
	 * @param d1 The first {@link DateTimeHolder} object.
	 * @param d2 The second {@link DateTimeHolder} object.
	 * @return <code>true</code> if <code>d1</code> is strictly before <code>d2</code>
	 */
	static boolean isBefore(final DateTimeHolder d1, final DateTimeHolder d2) {

		return new DateTime(d1.getTimestamp(), UTC) //
				.isBefore(new DateTime(d2.getTimestamp(), UTC));
	}

	/**
	 * Compare two {@link DateTimeHolder} objects.
	 * @param d1 The first {@link DateTimeHolder} object.
	 * @param d2 The second {@link DateTimeHolder} object.
	 * @return <code>true</code> if <code>d1</code> is strictly after <code>d2</code>
	 */
	static boolean isAfter(final DateTimeHolder d1, final DateTimeHolder d2) {

		return new DateTime(d1.getTimestamp(), UTC) //
				.isAfter(new DateTime(d2.getTimestamp(), UTC));
	}
}
